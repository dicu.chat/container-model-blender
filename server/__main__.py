from sanic.response import json
from sanic import Sanic
import ujson as js
import logging
import time

from .model import BlenderConfig, Blenderbot


app = Sanic(name="dicu-blender-ai")
config = BlenderConfig()
blender = Blenderbot.from_config(config)


@app.route("/api/get_response", methods=["POST"])
async def api_get_response(request):
    data = js.loads(request.json) if type(request.json) == str else request.json

    if not (user := data.get("user_id")):
        return json({"status": 500, "message": "You must provide 'user_id' with data."}, status=500)

    prompt = data.get("prompt", "").strip(" \n\r\t")
    # This is allowed intentionally, but lets still log it for now.
    logging.warning("Empty prompt value received over API. User '%s' ...", user)

    # Set/update history, if history is provided.
    if history := data.get("history", []):
        blender.set_initial_history(user, history[0])
        for message in history[1:]:  blender.update_chat_history(user, message)

    # Generate response.
    resp = (await blender.aget_response(user, prompt, data.get("input_ids_extern"))).strip(" \n\r\t")

    # Save history.
    blender.update_chat_history(user, f"U: {prompt}")
    blender.update_chat_history(user, f"B: {resp}")

    # Update history object:
    history.append(f"U: {prompt}")
    history.append(f"B: {resp}")

    return json({"timestamp": time.monotonic(), "text": resp, "history": history})


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8080)

from transformers import BlenderbotTokenizer, BlenderbotForConditionalGeneration
from collections import defaultdict
import asyncio
import logging
import torch
from transformers import BatchEncoding
import numpy as np

print("using CUDA =", torch.cuda.is_available())


class BlenderConfig:
    def __init__(
        self,
        name: str = "Blender",
        mname: str = "facebook/blenderbot-3B",
        persona: str = "your persona: I am a theatre performer.\nyour persona: I live in Dusseldorf.\n",
        max_position_embeddings: int = 128,
    ):
        self.blender_name: str    = name
        self.blender_mname: str   = mname
        self.blender_persona: str = persona
        self.blender_max_position_embeddings: int = max_position_embeddings


class Blenderbot:
    """
    This class contains the model and tokenizer from huggingface,
    custom persona, and chat history
    """

    def __init__(self, mname: str, name: str, persona: str, max_position_embeddings: int):
        logging.info("Loading blender model %s ...", mname)

        self.device = "cuda:0" if torch.cuda.is_available() else "cpu"

        self.name = name
        self.model = BlenderbotForConditionalGeneration.from_pretrained(mname).to(self.device)
        self.tokenizer = BlenderbotTokenizer.from_pretrained(mname)

        self.max_len_history = max_position_embeddings
        self.persona = persona
        self.chat_history = defaultdict(lambda: {
            "input_ids": torch.as_tensor([], dtype=torch.int).to(self.device),
            "attention_mask": torch.as_tensor([], dtype=torch.int).to(self.device)
        })

        self.history_clip_buffer  = 20  # clips the history a bit extra to allow for new response generation
        self.retained_history_idx = 0   # only clip history after here

    @classmethod
    def from_config(cls, config: BlenderConfig):
        return cls(
            mname = config.blender_mname,
            name = config.blender_name,
            persona = config.blender_persona,
            max_position_embeddings = config.blender_max_position_embeddings,
        )

    def hist_to_tensor(self, history):
        """
        converts history array to tensor for tokenizer.
        """
        for k in history:
            history[k] = torch.from_numpy(np.array(history[k]))
        return BatchEncoding(history)

    def set_persona(self, new_persona):
        """
        Set new persona, conforming to persona format in config
        """
        self.persona = new_persona

    def set_initial_history(self, user: str, convo_topic: str, return_history_idx: bool = False):
        """
        Set the history with the conversation topic and define retained_history_idx
        """
        persona_n_topic = self.persona + convo_topic + "\n"
        tokens = self.tokenizer([persona_n_topic], return_tensors="pt").to(self.device)

        self.retained_history_idx = tokens["input_ids"].shape[-1]
        self.chat_history[user] = tokens

        if return_history_idx:
            return self.retained_history_idx, self.chat_history[user]

    def update_chat_history(self, user: str, new_response: str):
        """
        adds new response to chat_history
        """
        response = self.tokenizer([new_response + "\n"], return_tensors="pt").to(self.device)  # input_ids and attention_mask dict
        for k in response:
            self.chat_history[user][k] = torch.cat([self.chat_history[user][k], response[k]], dim=-1).to(self.device)

        # clip history if necessary, and save new clipped history
        if self.chat_history[user]["input_ids"].shape[-1] >= self.max_len_history:
            # history set to clipped history and input
            self.chat_history[user] = self.clip_history(self.chat_history[user])

    def clip_history(self, inputs):
        logging.info("Clipping blender history ...")
        excess = inputs["input_ids"].shape[-1] - self.max_len_history + self.history_clip_buffer

        for k in inputs:  # inputs is dict consisting of input_ids and attention_mask tensors
            history_to_keep = inputs[k][:, :self.retained_history_idx]
            clipped_history = inputs[k][:, self.retained_history_idx + excess:]
            inputs[k] = torch.cat([history_to_keep, clipped_history], dim=-1).to(self.device)

        return inputs

    def get_response(self, user: str, input_text: str = "", input_ids_extern = None):
        """
        Generates response based on history and input_text
        --> keep input_text empty if updated history with input first
        """
        logging.info("Getting blender response ...")
        # add input text to chat history, or persona and input text in first round
        if input_text:
            inputs = self.tokenizer([input_text + "\n"], return_tensors="pt").to(self.device)

            # add chat history to input
            for k in inputs:
                inputs[k] = torch.cat([self.chat_history[user][k], inputs[k]], dim=-1).to(self.device)

            reply_ids = self.model.generate(**inputs)

        elif input_ids_extern is not None:
            input_ids_extern = self.hist_to_tensor(input_ids_extern)
            reply_ids = self.model.generate(**input_ids_extern)

        else:
            reply_ids = self.model.generate(**self.chat_history[user])

        response = self.tokenizer.batch_decode(reply_ids, skip_special_tokens=True)[0]
        return response

    async def aget_response(self, user: str, input_text: str = "", input_ids_extern = None):
        return await asyncio.get_event_loop().run_in_executor(
            None, self.get_response, user, input_text, input_ids_extern
        )


if __name__ == "__main__":
    config = BlenderConfig()
    user_id = "1234567890"

    testblender = Blenderbot.from_config(config)
    print(testblender.persona)

    def main():
        testblender.set_initial_history(user_id, "Topic: what's the point of existence?")
        for n in range(5):
            inp = input("you: ")
            resp = testblender.get_response(user_id, inp)
            resp = resp.strip(" \n\r\t")
            print("Blender:", resp)
            testblender.update_chat_history(user_id, "U: " + inp)
            testblender.update_chat_history(user_id, "B: " + resp)

    async def amain():
        async def bg():
            x = 0
            while True:
                print(f"\033[K\rSleep {x}s ...", end="")
                x += 1
                await asyncio.sleep(1)

        testblender.update_chat_history(user_id, "Topic: what's the point of existence?")
        for n in range(5):
            inp = input("you: ")

            t = asyncio.create_task(bg())
            resp = await testblender.aget_response(user_id, inp)
            t.cancel()

            resp = resp.strip(" \n\r\t")
            print("\033[K\rBlender:", resp)
            testblender.update_chat_history(user_id, "U: " + inp)
            testblender.update_chat_history(user_id, "B: " + resp)

    # main()
    asyncio.run(amain())
    print("history: ", testblender.tokenizer.decode(testblender.chat_history[user_id]["input_ids"][:][0], skip_special_tokens=True))

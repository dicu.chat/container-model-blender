FROM nvidia/cuda:11.4.2-runtime-ubuntu20.04

RUN apt-get -y update \
 && apt-get -y install python3 python3-pip

WORKDIR /app/usr/src

COPY requirements.txt .
RUN pip3 install --no-cache-dir -U pip wheel setuptools \
 && pip3 install --no-cache-dir -r requirements.txt

COPY server server
ENV CUDA_LAUNCH_BLOCKING=1
CMD ["python3", "-m", "server"]

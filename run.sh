PORT=8090
#NETWORK_EXTERNAL=--network=caddynet

# Note, requires nvidia runtime support - refer to README.md!
docker run --rm -d \
    --runtime=nvidia \
    -p $PORT:8080 $NETWORK_EXTERNAL \
    --name dicu-model-blender \
    dicu-model-blender

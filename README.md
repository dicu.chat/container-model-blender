<h1 align="center">
  <br>
  <a href="https://www.morphysm.com/"><img src="./assets/morph_logo_rgb.png" alt="Morphysm" ></a>
  <br>
  <h5 align="center"> Morphysm is a community of engineers, designers and researchers
contributing to security, cryptography, cryptocurrency and AI.</h5>
  <br>
</h1>

<h1 align="center">

 <img src="https://img.shields.io/badge/Python-3.9-brightgreen" alt="python badge">

 <img src="https://img.shields.io/badge/docker-20.10-blue" alt="docker badge">

 <img src="https://img.shields.io/badge/AI%20Model-Blender-red" alt="AI badge">
</h1> 


# Table of Contents

<!--ts-->

- [Getting Started](#getting-started)
- [Prerequisites](#prerequisites)
- [Installation](#installation)
- [Development](#development)
- [Testing The Model](#testing-the-model)
- [Demo](#demo)
- [TroubelShoting](#troubelShoting)
- [Code Owners](#code-owners)
- [License](#license)
- [Contact](#contact)
<!--te-->

# 1. Getting Started

This repository represents the Blender model container, which was proposed in [Recipes for building an open-domain chatbot](https://arxiv.org/pdf/2004.13637.pdf) on 30 Apr 2020.

To learn more about this model you can click [here!](https://huggingface.co/transformers/model_doc/blenderbot.html)

# 2. Prerequisites

- `docker`
- `docker-compose`
- `python3.9`
- Requirements to run (minimal):
    * ` 20GB RAM`
    * `4 VCPU`

- Requirements to run (recommended):
    * `32GB RAM`
    * `1 GPU (16GB VRAM total)`

# 3. Installation

1.  Download the repo, you can download our project either with SHH:

    ```bash
    git clone git@gitlab.com:dicu.chat/     container-model-blender.git
    ```

    or with HTTPS:

     ```bash
     https://gitlab.com/dicu.chat/container-model-blender.git
     ```

2. Download the docker: 

    ```bash 
     sudo apt-get install docker.io
    ```

3. Download the Docker-compose `v1.27+`:

   ```bash
   sudo apt-get install docker-compose
   ```

4. For Non-Docker:
   ```bash
    sudo apt install nvidia-cuda-toolkit virtualenv python3.8-dev
   ```

5. For Docker you will need to download the nvidia docker runtime follow this [link!](https://github.com/NVIDIA/nvidia-container-runtime).<br> </br>


6.  Run :
    ```bash
    ./build.sh && ./run.sh
    ```

# 4. Development

Please follow these steps to help you run our project:

1. Create dev python environment for the project:

    ```bash
    virtualenv -p python .venv && source .venv/bin/activate
    ```

    or run this command if you have not install the virtualenv:
    ```bash
    python -m venv .venv && source .venv/bin/activate
    ```

2. Install dependencies:

   ```bash
    python  -m pip install -r requirements.txt
   ```


3. Go to our [server project](https://gitlab.com/dicu.chat/server/-/tree/master) before you run this project, and then update the `BLENDER_URL` variable in the server repository .env file to reflect the local host and port, eg. `BLENDER_URL = http://0.0.0.0:8787`. also make sure to set the port in our [server/main.py, line 46](https://gitlab.com/dicu.chat/container-model-blender/-/blob/master/server/__main__.py#L46), so that there's no conflicts. <br> </br>

    *Note: manually change model size in [server/model.py Line 16](https://gitlab.com/dicu.chat/container-model-blender/-/blob/master/server/model.py#L16) if you don't much local compute power, eg. set to [this model](https://huggingface.co/facebook/blenderbot-400M-distill
) which should work for <16GB RAM. **Example:** `mname:str = "facebook/blenderbot-400M-distill"`* 
<br></br> 


4. Run the project:

   ```bash
    python -m server
   ```

# 5. Testing the Model

To put our model to the test and start a conversation with a Blender model's bot, Please run the [server repo](https://gitlab.com/dicu.chat/server) with this repo so they can start talking to each other. You must also run the [bridge telegram repo](https://gitlab.com/dicu.chat/bridge-telegram), but you must change the 'AI DEFAULT MODEL=blender' in the server repo's[.env file on line 49](https://gitlab.com/dicu.chat/server/-/blob/master/.env.example#L49). Then, go to your Telegram bot channel and type /start then /ai to begin a conversation with our AI bot blender model.



# 6. Demo

Will be updated soon...


# 7. TroubelShoting

If you have encountered any problems while running the code, please open a new issue in this repo and label it bug, and we will assist you in resolving it.


# 8. Code Owners

@morphysm/team :sunglasses:

# 9. License

Our reposiorty is licensed under the terms of the GPLv3 see the license [here!](https://gitlab.com/dicu.chat/container-model-blender/-/blob/master/LICENSE)

# 10. Contact

If you'd like to know more about us [Click here!](https://www.morphysm.com/), or You can contact us at "contact@morphysm.com".
